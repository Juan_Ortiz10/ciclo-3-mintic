from django.contrib import admin
from django.urls import path
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)
from TiendaApp import views
from TiendaApp.views.userCreateView import UserCreateView

urlpatterns = [
    path('login/', TokenObtainPairView.as_view()),
    path('refresh/', TokenRefreshView.as_view()),
    path('users/', views.UsersListView.as_view()),
    path('user/', views.UserCreateView.as_view()),
    path('user/<int:pk>/', views.UserRUDView.as_view()),
    path('categorias/', views.CategoriasListView.as_view()),
    path('categoria/', views.CategoriaCreateView.as_view()),
    path('categoria/<int:pk>/', views.CategoriaRUDView.as_view()),
    path('productos/', views.ProductosListView.as_view()),
    path('producto/', views.ProductoCreateView.as_view()),    
    path('producto/<int:pk>/', views.ProductoRUDView.as_view()),
    path('facturas/', views.FacturasListView.as_view()),
    path('factura/', views.FacturaCreateView.as_view()),
    path('factura/<int:pk>/', views.FacturaRDView.as_view()),
    path('ventas/', views.VentasListView.as_view()),
    path('venta/', views.VentaCreateView.as_view()),
    path('venta/<int:pk>/', views.VentaRDView.as_view()),
]
