from django.contrib import admin
from .models.user import User
from .models.categoria import Categoria
from .models.producto import Producto
from .models.factura import Factura
from .models.venta import Venta
admin.site.register(User)
admin.site.register(Categoria)
admin.site.register(Producto)
admin.site.register(Factura)
admin.site.register(Venta)