from django.db import models

class Categoria(models.Model):
    id = models.BigAutoField(primary_key=True)
    nombre = models.CharField('nombre', max_length = 50, null=False, default="")
    descripcion = models.CharField('descripcion', max_length = 100, null=False)