from django.db import models
from .user import User

class Factura(models.Model):
    id = models.BigAutoField(primary_key=True) 
    factura = models.FileField('factura',upload_to = 'facturas/', null=False)
    fecha = models.DateTimeField('fecha', max_length = 10, null=False, auto_now_add=True)  
    id_usuario = models.ForeignKey(User, related_name='id_usuario', on_delete=models.CASCADE)  