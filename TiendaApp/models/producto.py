from django.db import models
from .categoria import Categoria

class Producto(models.Model):
    id = models.AutoField(primary_key=True)   
    nombre = models.CharField('nombre', max_length=80, null=False)
    precio = models.IntegerField('precio', null=False, default=0)
    existencias = models.IntegerField('existencias', default=0, null=False)
    id_categoria = models.ForeignKey(Categoria, related_name='id_categoria', on_delete=models.CASCADE)
    