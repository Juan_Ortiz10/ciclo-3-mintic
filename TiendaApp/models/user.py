from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.contrib.auth.hashers import make_password

class UserManager(BaseUserManager):
    def create_user(self, nombre, password):
        user = self.model(nombre=nombre)
        user.set_password(password)
        user.save(using=self._db)
        return user
    def create_superuser(self, nombre, password):
        user = self.create_user(nombre=nombre,password=password)
        user.is_admin = True
        user.save(using=self._db)
        return user

class User(AbstractBaseUser, PermissionsMixin):
    id = models.BigAutoField(primary_key=True, null=False)
    nombre = models.CharField('nombre', max_length = 30, null=False, unique=True)
    password = models.CharField('password', max_length = 256)
    rol = models.CharField('rol', max_length=13, null=False)
    numero_celular = models.CharField('celular', max_length = 10, null=False)
    fecha_ingreso = models.DateField('fecha_ingreso', max_length = 10, null=False, auto_now_add=True)
    def save(self, **kwargs):
        some_salt = 'mMUj0DrIK6vgtdIYepkIxN'
        self.password = make_password(self.password, some_salt)
        super().save(**kwargs)
    objects = UserManager()
    USERNAME_FIELD = 'nombre'