from django.db import models
from .factura import Factura
from .producto import Producto

class Venta(models.Model):
    id = models.AutoField(primary_key=True)
    cantidad = models.IntegerField('cantidad',default=0, null=False)
    valor =  models.IntegerField('valor',default=0, null=False)
    fecha_venta = models.DateTimeField('fecha_venta', max_length = 10, null=False, auto_now_add=True)    
    id_factura = models.ForeignKey(Factura, related_name='id_factura', on_delete=models.CASCADE)
    id_producto = models.ForeignKey(Producto, related_name='id_producto', default=0, on_delete=models.CASCADE)