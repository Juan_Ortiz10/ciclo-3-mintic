from rest_framework import serializers
from TiendaApp.models.categoria import Categoria

class CategoriaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Categoria
        fields = ['id', 'nombre', 'descripcion']
        def create(self, validated_data):
            categoriaInstance = Categoria.objects.create(**validated_data)
            return categoriaInstance
        def to_representation(self, obj):
            categoria = Categoria.objects.get(id=obj.id)
            return {
            'id': categoria.id,
            'nombre': categoria.nombre,
            'descripcion': categoria.descripcion,
            }