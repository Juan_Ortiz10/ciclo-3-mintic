from rest_framework import serializers
from TiendaApp.models.factura import Factura
from TiendaApp.models.user import User

class FacturaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Factura
        fields = ['id', 'factura', 'fecha', 'id_usuario']
        def create(self, validated_data):
            facturaInstance = Factura.objects.create(**validated_data)
            return facturaInstance
        def to_representation(self, obj):
            factura = Factura.objects.get(id=obj.id)
            user = User.objects.get(id=obj.id_usuario)
            return {
            'id': factura.id,
            'factura': factura.factura,
            'fecha': factura.fecha,
            'usuario': {
                    'nombre': user.nombre,
                    'rol': user.rol,
                    'numero_celular': user.numero_celular
                }
            }
        