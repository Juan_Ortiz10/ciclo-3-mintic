from TiendaApp.models.producto import Producto
from rest_framework import serializers

class ProductoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Producto
        fields = ['id', 'nombre','precio', 'existencias', 'id_categoria']
        def create(self, validated_data):
            productoInstance = Producto.objects.create(**validated_data)
            return productoInstance
        def to_representation(self, obj):
            producto = Producto.objects.get(id=obj.id)
            return {
            'id': producto.id,
            'nombre': producto.nombre,
            'precio': producto.precio,
            'existencias': producto.existencias,
            'id_categoria': producto.id_categoria
            }