from rest_framework import serializers
from TiendaApp.models.user import User

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'nombre', 'password', 'rol', 'numero_celular', 'fecha_ingreso']
        def create(self, validated_data):
            userInstance = User.objects.create(**validated_data)
            return userInstance
        def to_representation(self, obj):
            user = User.objects.get(id=obj.id)
            return {
            'id': user.id,
            'nombre': user.nombre,
            'password': user.password,
            'rol': user.rol,
            'numero_celular': user.numero_celular,
            'fecha_ingreso': user.fecha_ingreso
            }
