from TiendaApp.models.venta import Venta
from rest_framework import serializers

class VentaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Venta
        fields = ['id', 'cantidad', 'valor', 'fecha_venta', 'id_factura', 'id_producto']
        def create(self, validated_data):
            ventaInstance = Venta.objects.create(**validated_data)
            return ventaInstance
        def to_representation(self, obj):
            venta = Venta.objects.get(id=obj.id)
            return {
                'id': venta.id,
                'cantidad': venta.cantidad,
                'valor': venta.valor,
                'fecha_venta': venta.fecha_venta,
                'id_factura': venta.id_factura,
                'id_producto': venta.id_producto
            }