from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from TiendaApp.serializers.categoriaSerializer import CategoriaSerializer

class CategoriaCreateView(generics.CreateAPIView):    
    serializer_class = CategoriaSerializer
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)