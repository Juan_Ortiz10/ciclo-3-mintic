from rest_framework import generics
from TiendaApp.models.categoria import Categoria
from TiendaApp.serializers.categoriaSerializer import CategoriaSerializer

class CategoriasListView(generics.ListAPIView):
    queryset = Categoria.objects.all()
    serializer_class = CategoriaSerializer
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)