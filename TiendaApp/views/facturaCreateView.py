from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from TiendaApp.serializers.facturaSerializer import FacturaSerializer

class FacturaCreateView(generics.CreateAPIView):
    serializer_class = FacturaSerializer
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)