from django.conf import settings
from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework_simplejwt.backends import TokenBackend
from TiendaApp.models.factura import Factura
from TiendaApp.serializers.facturaSerializer import FacturaSerializer

class FacturaRDView(generics.RetrieveDestroyAPIView):
    queryset = Factura.objects.all()
    serializer_class = FacturaSerializer
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)