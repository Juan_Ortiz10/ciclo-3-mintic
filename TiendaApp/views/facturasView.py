from rest_framework import generics
from TiendaApp.models.factura import Factura
from TiendaApp.serializers.facturaSerializer import FacturaSerializer

class FacturasListView(generics.ListAPIView):
    queryset = Factura.objects.all()
    serializer_class = FacturaSerializer
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)