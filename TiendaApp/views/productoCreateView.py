from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from TiendaApp.serializers.productoSerializer import ProductoSerializer

class ProductoCreateView(generics.CreateAPIView):
    serializer_class = ProductoSerializer
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)