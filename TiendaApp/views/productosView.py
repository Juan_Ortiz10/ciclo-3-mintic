from rest_framework import generics
from TiendaApp.models.producto import Producto
from TiendaApp.serializers.productoSerializer import ProductoSerializer

class ProductosListView(generics.ListAPIView):
    queryset = Producto.objects.all()
    serializer_class = ProductoSerializer
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)