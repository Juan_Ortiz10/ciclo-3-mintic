from django.conf import settings
from rest_framework import generics, status
from TiendaApp.models.user import User
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework_simplejwt.backends import TokenBackend
from TiendaApp.serializers.userSerializer import UserSerializer

class UsersListView(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)