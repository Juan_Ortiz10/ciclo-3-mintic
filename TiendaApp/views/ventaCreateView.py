from django.conf import settings
from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework_simplejwt.backends import TokenBackend
from TiendaApp.serializers.ventaSerializer import VentaSerializer

class VentaCreateView(generics.CreateAPIView):
    serializer_class = VentaSerializer
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)
