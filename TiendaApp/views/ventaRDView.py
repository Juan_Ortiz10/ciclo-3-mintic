from django.conf import settings
from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework_simplejwt.backends import TokenBackend
from TiendaApp.models.venta import Venta
from TiendaApp.serializers.ventaSerializer import VentaSerializer

class VentaRDView(generics.RetrieveDestroyAPIView):
    queryset = Venta.objects.all()
    serializer_class = VentaSerializer
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)